# pdf-client-wrapper

## 开发环境搭建

1. 拉取所有子仓库
1. 安装开发库
1. 执行脚本 `bin/compile_proto.sh`，编译 `proto` 文件

## 安装

注意替换安装命令中的版本号

```
# 使用pipenv
pipenv install "git+https://gitee.com/passer-team/pdf-client-wrapper.git@v0.0.2-py#egg=pdf-client-wrapper"

# 使用pip
pip install "git+https://gitee.com/passer-team/pdf-client-wrapper.git@v0.0.2-py#egg=pdf-client-wrapper"
```